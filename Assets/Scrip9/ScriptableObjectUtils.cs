﻿using UnityEngine;

namespace Scrip9
{
    [CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUnils")]
    public class ScriptableObjectUtils : ScriptableObject
    {
        public void Destroy(GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }
    }
}