﻿using Srip;
using Scrip5;
using Scrip6;
using UnityEngine;

namespace Scrip9
{
    [RequireComponent(typeof(ItemTYPE))]
    public class Pickupable : MonoBehaviour,IInteractable,IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
        {
            var itemtype = GetComponent<ItemTYPE>();

            var inventory = actor.GetComponent<IInventory>();
            inventory.AddItem(itemtype.Type.ToString(),1);
            
            Destroy(gameObject);
        }

        public void ActorEnter(GameObject actor)
        {
            
        }

        public void ActorExit(GameObject actor)
        {
            
        }
    }
}