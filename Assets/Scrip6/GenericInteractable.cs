﻿using Scrip5;
using UnityEngine;
using UnityEngine.Events;

namespace Scrip6
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();
        
        [SerializeField] protected UnityEvent<GameObject> m_OnInteractGameObject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorEnterGameObject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorExitGameObject = new();

        public virtual void Interact(GameObject actor)
        {
            if (m_OnInteract != null)
                m_OnInteract.Invoke();

            if (m_OnInteractGameObject != null)
                m_OnInteractGameObject.Invoke(actor);
        }

        public virtual void ActorEnter(GameObject actor)
        {
            if (m_OnActorEnter != null)
                m_OnActorEnter.Invoke();

            if (m_OnActorEnterGameObject != null)
                m_OnActorEnterGameObject.Invoke(actor);
        }

        public virtual void ActorExit(GameObject actor)
        {
            if (m_OnActorExit != null)
                m_OnActorExit.Invoke();

            if (m_OnActorExitGameObject != null)
                m_OnActorExitGameObject.Invoke(actor);
        }
        
    }
}