﻿using System;
using UnityEngine;

namespace Scrip6
{
    [RequireComponent(typeof(Rigidbody))]
    public class ObjectPusher : MonoBehaviour
    {
        [SerializeField] private float _forceMagnitude = 10;

        private Rigidbody _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Push(GameObject actor)
        {
            _rigidbody.AddForce(actor.transform.up*_forceMagnitude,ForceMode.Impulse);
        }
    }
}