using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Srip
{
    public class ItemTYPE : MonoBehaviour
    {
        public enum ItemType
        {
            COIN,
            BIGCOIN,
            POWERUP,
            POWERDOWN,
        }

        [SerializeField] protected ItemType m_ItemType;

        public ItemType Type
        {
            get { return m_ItemType; }
            set { m_ItemType = value; }
        }
    }
}
