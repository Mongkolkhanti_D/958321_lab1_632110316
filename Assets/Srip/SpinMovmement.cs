using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Srip
{
    public class SpinMovmement : MonoBehaviour
    {
        [SerializeField] private float m_AngularSpeed = 5.0f;

        [SerializeField] private Vector3 m_AxisOfRotation = new Vector3(1.0f, 0, 0);

        private Transform m_ObjTranform;
        
        // Start is called before the first frame update
        void Start()
        {
            m_ObjTranform = this.gameObject.GetComponent<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            m_ObjTranform.Rotate(m_AxisOfRotation, m_AngularSpeed);
        }
    }
}

