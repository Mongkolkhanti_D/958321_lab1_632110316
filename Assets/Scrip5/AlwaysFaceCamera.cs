﻿using System;
using UnityEngine;

namespace Scrip5
{
    public class AlwaysFaceCamera : MonoBehaviour
        {
            private void Update()
            {
                transform.rotation = Quaternion.LookRotation( transform.position - Camera.main.transform.position );
            }
        }
}
