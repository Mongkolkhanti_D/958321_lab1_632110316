﻿using UnityEngine;

namespace Scrip5
{
    public class Ch5CapsulePlayerController : PlayerController
    {
        public override void MoveForward()
        {
            transform.Translate(transform.forward*m_DirectionalSpeed*Time.deltaTime,Space.World);
        }
        
        public override void MoveForwardSprint()
        {
            transform.Translate(transform.forward*m_DirectionSprintSpeed*Time.deltaTime,Space.World);
        }
        
        public override void MoveBackward()
        {
           transform.Translate(-transform.forward*(m_DirectionalSpeed*0.4f)*Time.deltaTime,Space.World); 
        }
        
        public override void TurnLeft()
        {
            transform.Rotate(transform.up,-m_RotationSpeed*Time.deltaTime,Space.Self);
        }
        
        public override void TurnRight()
        {
            transform.Rotate(transform.up,m_RotationSpeed*Time.deltaTime,Space.Self);
        }

    }
}