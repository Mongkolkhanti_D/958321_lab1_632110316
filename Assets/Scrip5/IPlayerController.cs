﻿namespace Scrip5
{
    public interface IPlayerController
    {
            void MoveForward();
            void MoveForwardSprint();

            void MoveBackward();

            void TurnLeft();
            void TurnRight();
    }
}