﻿using UnityEngine;

namespace Scrip5
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}